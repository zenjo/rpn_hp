#!/usr/bin/perl -w
use strict;

my $licencetitre = "Licensing";
my $licencemsg = "
+-----------------------------------------------------------------------+
| Rpn_hP (Rpn_hP is Not HP)                                             |
| This program is free software: you can redistribute it and/or modify  |
| it under the terms of the GNU General Public License as published by  |
| the Free Software Foundation, either version 3 of the License, or     |
| (at your option) any later version.                                   |
|                                                                       |
| This program is distributed in the hope that it will be useful,       |
| but WITHOUT ANY WARRANTY; without even the implied warranty of        |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         |
| GNU General Public License for more details.                          |
|                                                                       |
| You should have received a copy of the GNU General Public License     |
| along with this program.  If not, see <http://www.gnu.org/licenses/>. |
|                                                                       |
| Author: Robert Sebille robert - AT - sebille - DOT - name             |
+-----------------------------------------------------------------------+
";

# Liste des modules installés
use Term::ReadKey qw(ReadMode ReadKey);
use Math::Trig qw(asin acos atan);

#################
# Configuration #
#################

our %T_; 
my $isolangue = "fr";
my $langues_dispo = "";
opendir(DIRHANDLE,"./lang")||die "Erreur: impossible de lire le répertoire ./lang\n";
foreach (readdir(DIRHANDLE)){
	$langues_dispo .= substr($_,-2).", " if (/^rpn_hp\.locale\...$/io);
}
closedir DIRHANDLE; 
$langues_dispo = substr($langues_dispo,0,length($langues_dispo)-2);

# Obligatoire: 4 minimum, 23 max. la longueur de la pile pour les HP = 4 registres T,Z,Y,X)
my $longpile = 4;
my $lgp = $longpile-1;
# pile et last X
my @pile = (0,0,0,0);
my $lastx = 0;
# Etat des mouvements de la pile (Voir Opérations)
my $mvpile="i";
# Memoires de la calculatrice
my %hpmem = ("m0",0, "m1",0, "m2",0, "m3",0, "m4",0, "m5",0, "m6",0, "m7",0, "m8",0, "m9",0);
# operateurs neutres à bascule
my $affiche_aide = 1;my $affiche_memoires = 0;

my $operator = "\n";my $typeoper = "i";my $operparam = "";

# hash du fichier de config
my %rpnhp_config;

my $dir_config = "$ENV{HOME}/.rpn_hp";
my $file_generic_config = "$dir_config/rpn_hp.conf";
if ( ! -e $dir_config) {
	mkdir($dir_config);
	if ( ! -w $dir_config) {chmod($dir_config, 0755) or die("Répertoire $ENV{HOME} protégé en écriture");}
	}

sub lisConfig {
	dbmopen(%rpnhp_config,$file_generic_config,0644) or die($!);
	$isolangue = $rpnhp_config{"isolangue"};
		if (!defined($isolangue)) {$isolangue="fr";$rpnhp_config{"isolangue"}=$isolangue;}
	$longpile = $rpnhp_config{"longpile"};if (!defined($longpile)) {$longpile=4;$rpnhp_config{"longpile"}=$longpile;}
	for (my $i=0;$i<$longpile;$i++) {
		$pile[$i] = $rpnhp_config{"pile$i"};if (!defined($pile[$i])) {$pile[$i]=0;$rpnhp_config{"pile$i"}=$pile[$i];}
		}
	$lastx = $rpnhp_config{"lastx"};if (!defined($lastx)) {$lastx=0;$rpnhp_config{"lastx"}=$lastx;}
	$mvpile = $rpnhp_config{"mvpile"};if (!defined($mvpile)) {$mvpile="i";$rpnhp_config{"mvpile"}=$mvpile;}
	$operator = $rpnhp_config{"operator"};if (!defined($operator)) {$operator="\n";$rpnhp_config{"operator"}=$operator;}
	$typeoper = $rpnhp_config{"typeoper"};if (!defined($typeoper)) {$typeoper="i";$rpnhp_config{"typeoper"}=$typeoper;}
	$operparam = $rpnhp_config{"operparam"};if (!defined($operparam)) {$operparam="";$rpnhp_config{"operparam"}=$operparam;}
	$hpmem{"m0"} = $rpnhp_config{"m0"};if (!defined($hpmem{"m0"})) {$hpmem{"m0"}=0;$rpnhp_config{"m0"}=$hpmem{"m0"};}
	$hpmem{"m1"} = $rpnhp_config{"m1"};if (!defined($hpmem{"m1"})) {$hpmem{"m1"}=0;$rpnhp_config{"m1"}=$hpmem{"m1"};}
	$hpmem{"m2"} = $rpnhp_config{"m2"};if (!defined($hpmem{"m2"})) {$hpmem{"m2"}=0;$rpnhp_config{"m2"}=$hpmem{"m2"};}
	$hpmem{"m3"} = $rpnhp_config{"m3"};if (!defined($hpmem{"m3"})) {$hpmem{"m3"}=0;$rpnhp_config{"m3"}=$hpmem{"m3"};}
	$hpmem{"m4"} = $rpnhp_config{"m4"};if (!defined($hpmem{"m4"})) {$hpmem{"m4"}=0;$rpnhp_config{"m4"}=$hpmem{"m4"};}
	$hpmem{"m5"} = $rpnhp_config{"m5"};if (!defined($hpmem{"m5"})) {$hpmem{"m5"}=0;$rpnhp_config{"m5"}=$hpmem{"m5"};}
	$hpmem{"m6"} = $rpnhp_config{"m6"};if (!defined($hpmem{"m6"})) {$hpmem{"m6"}=0;$rpnhp_config{"m6"}=$hpmem{"m6"};}
	$hpmem{"m7"} = $rpnhp_config{"m7"};if (!defined($hpmem{"m7"})) {$hpmem{"m7"}=0;$rpnhp_config{"m7"}=$hpmem{"m7"};}
	$hpmem{"m8"} = $rpnhp_config{"m8"};if (!defined($hpmem{"m8"})) {$hpmem{"m8"}=0;$rpnhp_config{"m8"}=$hpmem{"m8"};}
	$hpmem{"m9"} = $rpnhp_config{"m9"};if (!defined($hpmem{"m9"})) {$hpmem{"m9"}=0;$rpnhp_config{"m9"}=$hpmem{"m9"};}
	$affiche_aide = $rpnhp_config{"affiche_aide"};
		if (!defined($affiche_aide)) {$affiche_aide="1";$rpnhp_config{"affiche_aide"}=$affiche_aide;}
	$affiche_memoires = $rpnhp_config{"affiche_memoires"};
		if (!defined($affiche_memoires)) {$affiche_memoires="0";$rpnhp_config{"affiche_memoires"}=$affiche_memoires;}
	dbmclose(%rpnhp_config) or die($!);
	$lgp = $longpile-1;
	}

sub ecrisConfig {
	my ($cle,$valeur) = @_;
	dbmopen(%rpnhp_config,$file_generic_config,0644) or die($!);
	$rpnhp_config{$cle} = $valeur;
	dbmclose(%rpnhp_config) or die($!);
	}

##################
# Initialisation #
##################

lisConfig;

my $dbus_notify = 0;
if (system("which","notify-send")==0) {$dbus_notify = 1;}

#$isolangue = "en"; # Debug provisoire
if (-e "lang/rpn_hp.locale.$isolangue") {require "lang/rpn_hp.locale.$isolangue";} else {require "lang/rpn_hp.locale.fr"}

my $version = "Rpn_hP V. 0.16 [$isolangue]. Lic GNU/GPL 3. (c) Robert Sebille, $T_{'belgique'}, 10-2009.";

# Pour la lecture en console @opers contient les operateur qui stoppe la lecture, 
# $operator recoit l'operateur qui stoppe la lecture, $regx une serie de chiffre(s) avec 1 point.
# Types d'operation: 
# - Autorise (A) (@opers_a, $mvpile="a") les mvts de la piles: la pile monte lors de l'entrée suivante dans X 
# - Interdit (I) (@opers_i, $mvpile="i") les mvts de la piles: reste de la pile inchangée lors de l'entrée suivante dans X 
# - Neutre (N): (@opers_n) N'influence pas l'etat des mvts de la pile. 
my @opers_a = ("+","-","*","/","e","E","h","i","I","l","m","n","N","o","O","q","Q","r","s","S","c","C","t","T","w","=",">","<");
my @opers_i = ("z","\n","\r","\n\r");
my @opers_n = ("Z","a","A","d","D","F","L","M","p","P","R","!");
my @opers = (@opers_a,@opers_i,@opers_n);
# Operateurs forcant le last X
my @opers_lx = ("+","-","*","/","e","E","i","I","q","Q","n","N","o","O","s","S","c","C","t","T","=");
# Operateurs dit de rappel (recouvrement  last X, recall, introduction constante etc...)
my @opers_rpl = ("l","r","=");
# permet de gérer une erreur de paramètres sur ces opérateurs en empêchant la montee auto de la pile
my $opers_rpl_error = 0;
# Operateurs necessitant un parametre ((f)ixe les decimales, Fixe longueur pile, )
my @opers_param = ("F","L","m","r");

my %msg_opersparam = (
	"F"	=> "4-23",
	"L"	=> $langues_dispo,
	"m"	=> "0-9",
	"r"	=> "0-9"
	);

my $message = "";
my $titremessage = "";

# constante PI à 30 décimales
my $PI = 3.141592653589793238462643383279;

# Table de hashage des opérateurs non affichables
my %opertraduc = (
	"\n"     => "($T_{'entree'})",
	"\r"     => "($T_{'entree'})",
	"\n\r"   => "($T_{'entree'})",
	">"      => "> ($T_{'rotPileBas'})",
	"<"      => "< ($T_{'rotPileHaut'})",
	"a"      => "a ($T_{'affAideSuccincteOnOff'})",
	"A"      => "A ($T_{'aideComplete'})",
	"c"      => "c ($T_{'cosinus'})",
	"C"      => "C ($T_{'arcCosinus'})",
	"d"      => "d ($T_{'conversionRadiansDegres'})",
	"D"      => "D ($T_{'conversionDegresRadians'})",
	"e"      => "e ($T_{'xExposantY'})",
	"E"      => "E ($T_{'racineYdeX'})",
	"F"      => "F $T_{'fixeLongueurPileA'} ",
	"h"      => "h ($T_{'changementSigneX'})",
	"i"      => "i ($T_{'partieEntiereDeX'})",
	"I"      => "I ($T_{'partieFractionnaireDeX'})",
	"l"      => "l ($T_{'rappelLastX'})",
	"L"      => "L ($T_{'changeLaLangueVers'}) ",
	"m"      => "m $T_{'sauveXDansM'}",
	"M"      => "M ($T_{'affichageMemoiresOnOff'})",
	"n"      => "n ($T_{'lnX'})",
	"N"      => "N ($T_{'eExposantX'})",
	"o"      => "o ($T_{'logX'})",
	"O"      => "O ($T_{'dixExposantX'})",
	"q"      => "q ($T_{'xAuCarre'})",
	"Q"      => "Q ($T_{'racineCarreeDeX'})",
	"p"      => "p $T_{'convPolRect'}",
	"P"      => "P $T_{'convRectPol'}",
	"r"      => "r $T_{'rappelDansXdeM'}",
	"R"      => "R ($T_{'effacementDesMemoires'})",
	"s"      => "s ($T_{'sinus'})",
	"S"      => "S ($T_{'arcSinus'})",
	"t"      => "t ($T_{'tangente'})",
	"T"      => "T ($T_{'arcTangente'})",
	"w"      => "w ($T_{'echangeDesRegistresXetY'})",
	"z"      => "z ($T_{'miseAZeroDuRegistreX'})",
	"Z"      => "Z ($T_{'MiseAZeroDeTousLesRegistres'})",
	"="      => "p ($T_{'constantePi'})",
	"!"      => "! ($T_{'affichageLicence'})"
	);

# Aide succinte (les 2 variables aidetitre/msg ci-dessous pour compatibilite ascendante)
my $aidetitre = $T_{'aideTitre'};
my $aidemsg = $T_{'aideMsg'};

# Lettres des registres T,Z,Y,X propres aux calculatrices HP
my @registres_hp=("T","Z","Y","X");

#####################
# Saisie parametres #
#####################
my $param = join(" ",@ARGV);
# ne pas effacer l'ecran entre les operations si param -n ou --nocls (debugging)
my $clearscreen = 1; $clearscreen = 0 if ($param =~ m/-n|--nocls/);

###########################
# Fonctions mathematiques #
###########################

sub degRad {
	my $n = shift;
	return $n/180*$PI;
	}	

sub log10 {
	my $n = shift;
	return log($n)/log(10);
	}

sub polRect {
	my ($xa,$yd) = @_; # (Reg X angle en radian, Reg Y distance)
	my @xy;
	$xy[0] = $yd*cos($xa); # Reg X
	$xy[1] = $yd*sin($xa); # Reg Y
	return @xy;
	}	

sub radDeg {
	my $n = shift;
	return $n*180/$PI;
	}	

sub rectPol {
	my ($x,$y) = @_; # (Reg X = x, Reg Y = y)
	my @xayd;
# [0..2pi] Ref.: http://fr.wikipedia.org/wiki/Coordonnées_polaires
	# Reg X angle en radian
	if ($x>0 && $y>=0) {$xayd[0] = atan($y/$x);}
	if ($x>0 && $y<0) {$xayd[0] = atan($y/$x)+2*$PI;}
	if ($x<0) {$xayd[0] = atan($y/$x)+$PI;}
	if ($x==0 && $y>0) {$xayd[0] = $PI/2;}
	if ($x==0 && $y<0) {$xayd[0] = 3*$PI/2;}
	if ($x==0 && $y==0) {$xayd[0] = 0;}
	$xayd[1] = ($x**2+$y**2)**(1/2); # Reg Y distance
	return @xayd;
	}	

####################
# Gestion memoires #
####################

my $memtitre="Liste des mémoires"; my $memmsg;
sub lisMemoires {
	my @keys_sorted = sort {$a cmp $b } keys(%hpmem);
	$memmsg="\n-------------------\n";
	foreach my $v (@keys_sorted) {$memmsg.="$v: $hpmem{$v}\n";}
	}

#init
lisMemoires;

sub ecrisMemoire {
	my $poub;my $contmem;
	if (!($operparam=~m/^[+-\/\*]?[0-9]{1}$/)) 
		{$titremessage="$T_{'erreur'}";$message="$T_{'pasDeMemoireM'}$operparam";}
	else {
		if ($operparam=~m/^[0-9]{1}$/) {$poub="m$operparam";$contmem=$pile[$lgp];}
			else {
			$poub=chop($operparam);my $opermem=chop($operparam);
			$operparam=$poub; # utile pour l'affichage de imprimePile
			$poub="m$operparam";
			if ($opermem eq "+") {$contmem=$hpmem{$poub}+$pile[$lgp];}
			if ($opermem eq "-") {$contmem=$hpmem{$poub}-$pile[$lgp];}
			if ($opermem eq "/") {
				if ($pile[$lgp] == 0) {
					$contmem=$hpmem{$poub};
					$titremessage="$T_{'erreur'}";$message="$T_{'divisionParZeroInterdite'}";}
				else {$contmem=$hpmem{$poub}/$pile[$lgp];}
				}
			if ($opermem eq "*") {$contmem=$hpmem{$poub}*$pile[$lgp];}
			} 
		ecrisConfig($poub,$contmem);$hpmem{$poub}=$contmem;
		}
	}

sub rappelMemoire {
	if ($opers_rpl_error==1) { # opers_rpl_error est traité dans gereEtatMvtPile un peu plus loin
		$titremessage="$T_{'erreur'}";$message="$T_{'pasDeMemoireM'}$operparam";
		$opers_rpl_error=0;
		}
	else {my $poub="m$operparam";$pile[$lgp]=$hpmem{$poub};}
	}

sub clearMemoires {
	%hpmem = ("m0",0, "m1",0, "m2",0, "m3",0, "m4",0, "m5",0, "m6",0, "m7",0, "m8",0, "m9",0);
	my $poub;
	for (my $i=0;$i<=9;$i++) {$poub="m$i";ecrisConfig($poub,0);}
	}

###################
# Gestion de pile #
###################

# Met tous les registres de la pile et last X a 0. Remet la pile a 0 dans la config
sub clearPile {
	@pile = ();
	for (my $i=1;$i<=$longpile;$i++) {push(@pile,0);}
	$lastx=0;
	for (my $i=0;$i<=22;$i++) {ecrisConfig("pile$i",0);}
	}


# Règle la longueur de la pile des registres a $longpile
sub longPile {
	if (@pile<$longpile) {
		my $i=@pile;
		until ($i>=$longpile) {unshift(@pile,$pile[0]);$i++;}
		}
	if (@pile>$longpile) {splice(@pile,0,-$longpile);}
}

# Imprime la pile des registres et sauve la pile dans la configuration
sub imprimePile {

	if ($clearscreen == 1) {my $clear_string = `clear`;print $clear_string;} else {print "\n\n";}

	print "$version\n$T_{'underConstruction'}";
	my $i=0; 
	foreach my $reg (@pile) {
		my $nomreg = $registres_hp[0];
		if ($i>0 && $i<($longpile-3)) {$nomreg = chr($i+64);};
		if ($i>=($longpile-3) && $i<($longpile)) {my $poub=$i-($longpile-4); $nomreg = $registres_hp[$poub];}
		print "\n$T_{'reg'} $nomreg: $reg";
		$i++;
		}
	print "\n\n$T_{'lastX'}: $lastx";
	print " | $T_{'lastOp'}: ";
	if(exists($opertraduc{$operator})) {print $opertraduc{$operator};} else {print $operator;}
	my $verif_operparam=0; foreach my $v (@opers_param) {if ($operator eq $v) {$verif_operparam=1;}}
	if ($verif_operparam==1) {print $operparam;}
	print " | $T_{'typeOp'}: $typeoper | a $T_{'aide'} | ! $T_{'licence'} | ESC $T_{'quitter'}\n";
	if ($dbus_notify==0) {
		if ($titremessage ne "") {print "$titremessage: ";}
		if ($message ne "") {print "$message";}
		}
	else {
		if ($titremessage ne "" || $message ne "") {system("notify-send","-t","5000","-i","$ENV{PWD}/icones/calculette.png",$titremessage,$message);}
		}
	$titremessage = "";$message = "";

	if ($affiche_memoires==1) {lisMemoires;print "\n$memtitre: $memmsg\n";}
	if ($affiche_aide==1) {print "\n$aidetitre: $aidemsg\n";}

	#sauve la pile dans la configuration
	for (my $i=0;$i<$longpile;$i++) {ecrisConfig("pile$i",$pile[$i]);}
	for (my $i=$longpile;$i<=22;$i++) {ecrisConfig("pile$i",0);}
	ecrisConfig("lastx",$lastx);
	ecrisConfig("mvpile",$mvpile);
	ecrisConfig("operator",$operator);
	ecrisConfig("typeoper",$typeoper);
	ecrisConfig("operparam",$operparam);

}

# Pousse la pile des registres vers le haut
sub montePile {push(@pile,undef);longPile;}

# gere les mvts automatiques de la pile à l'entree d'un nombre dans le reg X (qui depend de l'etat de mvt de la pile) 
sub gereMvtAutoPile {if ($mvpile eq "a") {montePile;}}

# sauve lastX, gere les mouvements de la pile dans le cas d'un opérateur de rapppel (lastx, recall, ... )
# et enregistre le nouvel etat de mvt automatique de la pile d'après l'operateur actuel.
sub gereEtatMvtPile {

	# Traitement de last X et sauvetage suivant l'operateur actuel
	foreach my $v (@opers_lx) {$lastx = $pile[$lgp] if ($operator eq $v);}

	# Gestion oper de rpl L'etat du mvt de la pile est toujours le precedent
	$opers_rpl_error = ($operator eq "r" && !($operparam=~m/^[0-9]{1}$/));
	foreach my $v (@opers_rpl) 
		{montePile if ($operator eq $v && $mvpile eq "a" && $opers_rpl_error == 0);}
	
	# suite: sauvetage de l'etat du mvt de la pile de l'operation courante
	foreach my $v (@opers_a) {if ($operator eq $v) {$mvpile = "a";$typeoper = "(a)";}}
	foreach my $v (@opers_i) {if ($operator eq $v) {$mvpile = "i";$typeoper = "(i)";}}
	my $opneutre = "non"; foreach my $v (@opers_n) {$opneutre = "oui" if ($operator eq $v);}
	if ($opneutre eq "oui") {$typeoper = "(n) $T_{'etatPile'}: ($mvpile)";}

}


##############
# Operations #
##############
# Types d'operation et mouvements automatiques de la pile: 
# - Autorise (A) ($mvpile="a") les mvts de la piles: si nombre pileMonte lors de l'entrée suivante dans X 
# - Interdit (I) ($mvpile="i") les mvts de la piles: si nombre l'entrée suivante dans X ecrase le registre, reste de la pile inchangé
# - Neutre (N): N'influence pas l'etat des mvts de la pile.
# - modifie last X (LX)
# Entree	I   
# z CLX (X=0)	I
# +		A	LX
# -		A	LX
# *		A	LX
# /		A	LX
# c Cin X	A	LX
# C Acos X	A	LX
# e X exp Y	A	LX
# E X rac Y	A	LX
# i int X	A	LX
# I frac X	A	LX
# n ln X	A	LX
# N e exp X	A	LX
# o log X	A	LX
# O 10 exp X	A	LX
# q X exp 2	A	LX
# Q X rac 2	A	LX
# s Sin X	A	LX
# S Asin X	A	LX
# t Tang X	A	LX
# T Atang X	A	LX
# = PI		A	LX
# h CHS		A
# l Last X	A
# m[+-/*]X>m0/9	A
# r m0/9 > X	A
# w Swap X Y	A
# > Rotat. bas	A
# < Rotat. haut	A
# a aide	N
# A AIDE	N
# d X rad>deg	N
# D X deg>rad	N
# F Long pile	N
# L Change lang	N
# M Aff. mem.	N
# p pol > rect	N
# P rect > pol	N
# R Clear MEM	N
# Z Clear REG	N
# ! Licence	N

sub gereOperation {
# Operateurs i
	# Entree
	if ($operator =~ m/\n|\r|\n\r/) {push(@pile,$pile[$lgp]);longPile;}
	# z CLX (X=0)
	if ($operator eq "z") {$pile[$lgp]=0;longPile;}

# Operateurs a avec LastX
	# + LX
	if ($operator eq "+") {my $rx=pop(@pile);my $ry=pop(@pile);push(@pile,$ry+$rx);longPile;}
	# - LX
	if ($operator eq "-") {my $rx=pop(@pile);my $ry=pop(@pile);push(@pile,$ry-$rx);longPile;}
	# * LX
	if ($operator eq "*") {my $rx=pop(@pile);my $ry=pop(@pile);push(@pile,$ry*$rx);longPile;}
	# / LX
	if ($operator eq "/") {
		if ($pile[$lgp] == 0) {$titremessage="$T_{'erreur'}";$message="$T_{'divisionParZeroInterdite'}";}
			else {my $rx=pop(@pile);my $ry=pop(@pile);push(@pile,$ry/$rx);longPile;}
		}
	# c Cosinus X LX
	if ($operator eq "c") {$pile[$lgp]=cos($pile[$lgp]);longPile;}
	# C ArcCosinus X  LX
	if ($operator eq "C") {$pile[$lgp]=acos($pile[$lgp]);longPile;}
	# e (X exp Y) LX
	if ($operator eq "e") {$pile[$lgp]=$pile[$lgp]**$pile[$lgp-1];longPile;}
	# E (X rac Y) LX
	if ($operator eq "E") {
		if ($pile[$lgp-1] == 0) 
			{$titremessage="$T_{'erreur'}";$message="$T_{'impossibleDeTrouverRacineZero'}";}
		elsif ($pile[$lgp] < 0) {
			$titremessage="$T_{'erreur'}";
			$message="$T_{'jeNeGerePasLaRacine'} ".$pile[$lgp-1]."$T_{'emeDunNombreNegatif'}";
			}
		else {$pile[$lgp]=$pile[$lgp]**(1/$pile[$lgp-1]);longPile;}
		}
	# i int X LX (int() n'est pas tres fiable d'apres la doc perl)
#	if ($operator eq "i") {$pile[$lgp]=int($pile[$lgp]);longPile;}
	if ($operator eq "i") {$pile[$lgp] =~ s/(-?[0-9]*)\.[0-9]*/$1/;longPile;}
	# I frac X LX
#	if ($operator eq "I") {$pile[$lgp]=$pile[$lgp]-int($pile[$lgp]);longPile;}
	if ($operator eq "I") {
		my $int_pile = $pile[$lgp];
		$int_pile =~ s/(-?[0-9]*)\.[0-9]*/$1/;
		$pile[$lgp] = $pile[$lgp]-$int_pile;
		longPile;
		}
	# n ln X  LX
	if ($operator eq "n") {$pile[$lgp]=log($pile[$lgp]);longPile;}
	# N e exp X  LX
	if ($operator eq "N") {$pile[$lgp]=exp($pile[$lgp]);longPile;}
	# o log X  LX
	if ($operator eq "o") {$pile[$lgp]=log10($pile[$lgp]);longPile;}
	# O 10 exp X  LX
	if ($operator eq "O") {$pile[$lgp]=10**$pile[$lgp];longPile;}
	# q (X exp 2) LX
	if ($operator eq "q") {$pile[$lgp]=$pile[$lgp]**2;longPile;}
	# Q (X rac 2) LX
	if ($operator eq "Q") {
		if ($pile[$lgp] < 0) 
			{$titremessage="$T_{'erreur'}";$message="$T_{'jeNeGerePasLaRacineCarreeDunNombreNegatif'}";}
		else {$pile[$lgp]=$pile[$lgp]**(1/2);longPile;}
		}
	# s Sinus X LX
	if ($operator eq "s") {$pile[$lgp]=sin($pile[$lgp]);longPile;}
	# S ArcSinus X  LX
	if ($operator eq "S") {$pile[$lgp]=asin($pile[$lgp]);longPile;}
	# t Tangente X LX
	if ($operator eq "t") {$pile[$lgp]=tan($pile[$lgp]);longPile;}
	# T ArcTangente X LX
	if ($operator eq "T") {$pile[$lgp]=atan($pile[$lgp]);longPile;}
	# = PI LX
	if ($operator eq "=") {$pile[$lgp]=$PI;longPile;}

# Operateurs a sans LastX
	# h CHS
	if ($operator eq "h") {$pile[$lgp]=-$pile[$lgp];longPile;}
	# l operateur de rappel du registre Last X
	if ($operator eq "l") {$pile[$lgp]=$lastx;longPile;}
	# m Sauve X dans m.$operparam = [0-9]
	if ($operator eq "m") {ecrisMemoire;}
	# r Rappelle le contenu de m.$operparam = [0-9] dans X
	if ($operator eq "r") {rappelMemoire;}
	# w Swap X Y
	if ($operator eq "w") {($pile[$lgp-1],$pile[$lgp])=($pile[$lgp],$pile[$lgp-1]);longPile;}
	# > Rotation de la pile par le bas
	if ($operator eq ">") {unshift(@pile,$pile[$lgp]);my $poub = pop(@pile);longPile;}
	# > Rotation de la pile par le haut
	if ($operator eq "<") {push(@pile,$pile[0]);longPile;}

# Operateurs n
	# a Aide succinte
	if ($operator eq "a") {
		if ($affiche_aide==1) {$affiche_aide=0;} else {$affiche_aide=1;}
		ecrisConfig("affiche_aide",$affiche_aide);
		}
	# A AIDE complète
	if ($operator eq "A") {
		if (system("which","less")==0) {system("less","-ig","lang/rpn_hp_aide_complete.$isolangue");}
		else {$titremessage="$T_{'erreur'}";$message="$T_{'pagerLess'}";}
		}
	# d X rad > deg
	if ($operator eq "d") {$pile[$lgp]=radDeg($pile[$lgp]);longPile;}
	# D X deg > rad
	if ($operator eq "D") {$pile[$lgp]=degRad($pile[$lgp]);longPile;}
	# F XX Fixe la longueur et sauve de la pile
	if ($operator eq "F") {
		if ($operparam =~ m/[0-9]/) {$longpile=$operparam;} 
		else {$titremessage="$T_{'erreur'}";$message="$operparam $T_{'pasUnNombrePileInchangee'}";}
		if ($longpile<4) {
			$longpile=4;
			$titremessage="$T_{'erreur'}";$message="$operparam $T_{'PlusPetitQue4PileRameneeA4'}";
			}
		if ($longpile>23) {
			$longpile=23;
			$titremessage="$T_{'erreur'}";$message="$operparam $T_{'plusGrandQue23PileTronqueeA23'}";
			}
		$lgp=$longpile-1;
		ecrisConfig("longpile",$longpile);
		longPile;
		}
	# L Change langue
	if ($operator eq "L") {
		if (!($langues_dispo =~ m/^.*$operparam.*$/)) {
			$titremessage="$T_{'erreur'}";
			$message="$operparam $T_{'pasDansLesLanguesDispo'}: $langues_dispo";
			$operparam=$isolangue;
			}
		else {
			$isolangue=$operparam;ecrisConfig("isolangue",$isolangue);$titremessage="$T_{'attention'}";
			$message="$T_{'relancerCalulatricePourActiverLaLangue'} $operparam";
			}
		}
	# M Affichage des memoires
	if ($operator eq "M") {
		if ($affiche_memoires==1) {$affiche_memoires=0;} else {$affiche_memoires=1;}
		ecrisConfig("affiche_memoires",$affiche_memoires);
		}
	# p pol > rect	
	if ($operator eq "p") {my @poub=polRect($pile[$lgp],$pile[$lgp-1]);$pile[$lgp]=$poub[0];$pile[$lgp-1]=$poub[1];longPile;}
	# P rect > pol	
	if ($operator eq "P") {my @poub=rectPol($pile[$lgp],$pile[$lgp-1]);$pile[$lgp]=$poub[0];$pile[$lgp-1]=$poub[1];longPile;}
	# R Effacement des memoires
	if ($operator eq "R") {clearMemoires;lisMemoires;}
	# Z Met tous les registres de la pile et last X a 0. Remet la pile a 0 dans la config
	if ($operator eq "Z") {clearPile;}
	# ! Affichage licence
	if ($operator eq "!") {$titremessage=$licencetitre;$message=$licencemsg;}
}

########
# Main #
########
#boucle de lecture console: serie de chiffres terminee par un operateur @opers

imprimePile;
my $key = "";
my $nb_frappe = 0;

open(TTY, "</dev/tty");
ReadMode "raw";
until ($key eq "\x1B") { # \x1B = ESC
	my $chiffre_point = undef;
	$key = ReadKey 0, *TTY;
	# On entre un chiffre ou un (et un seul) point
	if ($nb_frappe==0) {$chiffre_point = ($key =~ m/[0-9]/ || $key =~ m/\./);}
		else {$chiffre_point = ($key =~ m/[0-9]/ || ($key =~ m/\./ && !($pile[$lgp] =~ m/\./)));}
	if ($chiffre_point) {
		$nb_frappe++;
		# mouvement eventuel de la pile, avant la 1ere entree dans le reg X et maj du reg X
		# si la 1ere entree est un ".", on ecrit "0."
		if ($nb_frappe == 1) {gereMvtAutoPile; if ($key eq ".") {$pile[$lgp]="0".$key;} else {$pile[$lgp]=$key;}}
			else {$pile[$lgp]=$pile[$lgp].$key;}
		}
	# Backspace
	if ($key eq "\x7F" && length($pile[$lgp]) > 1) {my $poub = chop($pile[$lgp]);imprimePile;}

	# On entre autre chose q'un chiffre ou un point => 
	# gestion des operateurs: on termine la saisie si c'en est un, et on l'execute.
	foreach my $val (@opers) {
		if ($key eq $val) {
			$operator=$key;
			# Cet operateur a-t-il besoin d'un parametre?
			my $verif_operparam=0; foreach my $v (@opers_param) {if ($operator eq $v) {$verif_operparam=1;}}
			if ($verif_operparam==1) {
				print "\n------------------------------\n$T_{'parametrePourOperateur'} \"$operator\"";
				print " ($opertraduc{$operator})?";
				print " [$msg_opersparam{$operator}], $T_{'puis'} $T_{'entree'}: ";
				$operparam = <STDIN>;print $operparam;my $poub = chop($operparam);
				}
			gereEtatMvtPile;
			gereOperation;
			$nb_frappe = 0;
			} # if ($key eq $val)
		} # foreach my $val (@opers)
	imprimePile;
	} # until ($key eq "\x1B") ESC
ReadMode "normal";
print "\n";




