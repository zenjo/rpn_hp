#Rpn_hP is Not _HP
Rpn_hP V. 0.16. Lic GNU/GPL 3. (c) Robert Sebille, Belgium, 10-2009.

This program will only work on Linux / unix

it uses perl modules:
use Term:: ReadKey qw (ReadMode ReadKey);
use Math:: Trig qw (asin acos atan);

it uses these Linux programs:
- Detection of installation of the pager less for the full help, otherwise error message
- Using dbus for error messages if notify-send was found by "which". If not, displays the message on the console.
(notify-send is in the package libnotify or libnotify-bin following your Linux distribution).

### Installation:
```
- Unpack the archive in any directory of your Home
- Chmod +x *.pl
- Launch the calculator: ./rpn_hp.pl ou perl rpn_hp.pl
The first launch initializes the configuration in ~/.rpn_hp
```

### Uninstall:
```
- Cd ~
- Rm <dir_where_is_rpn_hp.pl>/*
- Rmdir <dir_where_is_rpn_hp.pl>
- Rm .rpn_hp/*
- Rmdir .rpn_hp
```
Reads and displays the configuration saved in dbm format in ~/.rpn_hp/
```
./rpn_hp_lis_config.pl
```
Launch rpn_hp.pl from any directory that you are authorized.

```
./rpn_hp.sh
```
### To configure:
```
rpn_hp_dir = "<dir_where_is_rpn_hp.pl>";

= <Le_rep_ou_est_rpn_hp.pl> From your home
Example:
rpn_hp_dir = "dev/perl/rpn_hp";
(suppose that rpn_hp.pl is in ~/dev/perl/rpn_hp)
```

Can then be incorporated into a launcher menu, for example (run in terminal).

### Aide
Vous pouvez télécharger les fichiers d'aide complète à partir de cette page: https://framagit.org/zenjo/rpn_hp/tree/master/lang, rpn_hp_aide_complete.fr ou rpn_hp_aide_complete.en

