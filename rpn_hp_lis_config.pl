#!/usr/bin/perl -w
use strict;

our (%T_,%T2_);
my $dir_config = "$ENV{HOME}/.rpn_hp";
my $file_generic_config = "$dir_config/rpn_hp.conf";
if ( ! -e $dir_config) {die("Ne trouve pas le répertoire de configuration $ENV{HOME}/.rpn_hp");}
	else {my $clear_string = `clear`;print $clear_string;}

my ($isolangue,@pile,$lastx,$mvpile,$longpile,$affiche_aide,$affiche_memoires,%hpmem,%rpnhp_config,$operator,$typeoper,$operparam);

sub lisConfig {
	dbmopen(%rpnhp_config,$file_generic_config,0644) or die($!);
	$isolangue = $rpnhp_config{"isolangue"};
	$longpile = $rpnhp_config{"longpile"};
	for (my $i=0;$i<=22;$i++) {$pile[$i] = $rpnhp_config{"pile$i"};}
	$lastx = $rpnhp_config{"lastx"};
	$mvpile = $rpnhp_config{"mvpile"};
	$operator = $rpnhp_config{"operator"};
	$typeoper = $rpnhp_config{"typeoper"};
	$operparam = $rpnhp_config{"operparam"};
	$hpmem{"m0"} = $rpnhp_config{"m0"};
	$hpmem{"m1"} = $rpnhp_config{"m1"};
	$hpmem{"m2"} = $rpnhp_config{"m2"};
	$hpmem{"m3"} = $rpnhp_config{"m3"};
	$hpmem{"m4"} = $rpnhp_config{"m4"};
	$hpmem{"m5"} = $rpnhp_config{"m5"};
	$hpmem{"m6"} = $rpnhp_config{"m6"};
	$hpmem{"m7"} = $rpnhp_config{"m7"};
	$hpmem{"m8"} = $rpnhp_config{"m8"};
	$hpmem{"m9"} = $rpnhp_config{"m9"};
	$affiche_aide = $rpnhp_config{"affiche_aide"};
	$affiche_memoires = $rpnhp_config{"affiche_memoires"};
	dbmclose(%rpnhp_config) or die($!);
	}

lisConfig;

my $langues_dispo = "";
opendir(DIRHANDLE,"./lang")||die "Erreur: impossible de lire le répertoire ./lang\n";
foreach (readdir(DIRHANDLE)){
	$langues_dispo .= substr($_,-2).", " if (/^rpn_hp\.locale\...$/io);
}
closedir DIRHANDLE; 
$langues_dispo = substr($langues_dispo,0,length($langues_dispo)-2);

if (-e "lang/rpn_hp.locale.$isolangue") {require "lang/rpn_hp.locale.$isolangue";} else {require "lang/rpn_hp.locale.fr"}
if (-e "lang/rpn_hp.locale.$isolangue") {require "lang/rpn_hp_lis_config.locale.$isolangue";} 
	else {require "lang/rpn_hp_lis_config.locale.fr"}

# Lettres des registres T,Z,Y,X propres aux calculatrices HP
my @registres_hp=("T","Z","Y","X");


# Table de hashage des opérateurs non affichables
my %opertraduc = (
	"\n"     => "($T_{'entree'})",
	"\r"     => "($T_{'entree'})",
	"\n\r"   => "($T_{'entree'})",
	">"      => "> ($T_{'rotPileBas'})",
	"<"      => "< ($T_{'rotPileHaut'})",
	"a"      => "a ($T_{'affAideSuccincteOnOff'})",
	"A"      => "A ($T_{'aideComplete'})",
	"c"      => "c ($T_{'cosinus'})",
	"C"      => "C ($T_{'arcCosinus'})",
	"d"      => "d ($T_{'conversionRadiansDegres'})",
	"D"      => "D ($T_{'conversionDegresRadians'})",
	"e"      => "e ($T_{'xExposantY'})",
	"E"      => "E ($T_{'racineYdeX'})",
	"F"      => "F $T_{'fixeLongueurPileA'} ",
	"h"      => "h ($T_{'changementSigneX'})",
	"i"      => "i ($T_{'partieEntiereDeX'})",
	"I"      => "I ($T_{'partieFractionnaireDeX'})",
	"l"      => "l ($T_{'rappelLastX'})",
	"m"      => "m $T_{'sauveXDansM'}",
	"M"      => "M ($T_{'affichageMemoiresOnOff'})",
	"n"      => "n ($T_{'lnX'})",
	"N"      => "N ($T_{'eExposantX'})",
	"o"      => "o ($T_{'logX'})",
	"O"      => "O ($T_{'dixExposantX'})",
	"q"      => "q ($T_{'xAuCarre'})",
	"Q"      => "Q ($T_{'racineCarreeDeX'})",
	"p"      => "p $T_{'convPolRect'}",
	"P"      => "P $T_{'convRectPol'}",
	"r"      => "r $T_{'rappelDansXdeM'}",
	"R"      => "R ($T_{'effacementDesMemoires'})",
	"s"      => "s ($T_{'sinus'})",
	"S"      => "S ($T_{'arcSinus'})",
	"t"      => "t ($T_{'tangente'})",
	"T"      => "T ($T_{'arcTangente'})",
	"w"      => "w ($T_{'echangeDesRegistresXetY'})",
	"z"      => "z ($T_{'miseAZeroDuRegistreX'})",
	"Z"      => "Z ($T_{'MiseAZeroDeTousLesRegistres'})",
	"="      => "p ($T_{'constantePi'})",
	"!"      => "! ($T_{'affichageLicence'})"
	);

sub printConfig {
	print "$T2_{'underConstruction'}";
	print "\n$T2_{'bordConfActRpnhP'}\n";
	print "$T2_{'configurationRpnhP'}\n";
	print "$T2_{'bordConfActRpnhP'}\n";
	print "| ";
	system("date");
	print "$T2_{'codeIsoLangue'} = $isolangue\n";
	print "$T2_{'languesDisponiblesCalculatrice'} = $langues_dispo\n";
	print "\n$T2_{'etatDeLaPile'}:\n";
	print "$T2_{'soulEtatDeLaPile'}\n";
	print "  $T2_{'longueurPile'}: = $longpile\n";
	print "  $T2_{'valeursPile'} =";
	my $i=0; 
	foreach my $reg (@pile) {
		my $nomreg = $registres_hp[0];
		if ($i>0 && $i<($longpile-3)) {$nomreg = chr($i+64);};
		if ($i>=($longpile-3) && $i<($longpile)) {my $poub=$i-($longpile-4); $nomreg = $registres_hp[$poub];}
		print "\n     $T_{'reg'} $nomreg = $reg";
		$i++; last if ($i==$longpile);
		}
	print "\n";
	print "  $T2_{'valeurLastX'} = $lastx\n";
	print "  $T2_{'etatMouvementPile'} = $mvpile\n";
	print "  $T2_{'dernierOperateur'} = ";
	if(exists($opertraduc{$operator})) {print $opertraduc{$operator};} else {print $operator;}
	print "\n";
	print "  $T2_{'typeDernierOperateur'} = $typeoper\n";
	print "  $T2_{'paramDernierOperateurAParam'} = $operparam\n";
	print "  $T2_{'etatAffPermAideSuccincte'} = $affiche_aide\n";
	print "  $T2_{'etatAffPermListeMemoires'} = $affiche_memoires\n";
	# Memoires
	my @keys_sorted = sort {$a cmp $b } keys(%hpmem);
	my $memmsg="\n$T2_{'valeursMemoires'}:\n";
	$memmsg.="$T2_{'soulValeursMemoires'}\n";
	foreach my $v (@keys_sorted) {$memmsg.="  $v = $hpmem{$v}\n";}
	print $memmsg;

	}

########
# Main #
########

printConfig;

