#!/bin/bash

############################################################
# Lanceur rpn_hp.pl. Configurez ceci:                      #
# Le repertoire ou se trouve rpn_hp à partir de votre home #

rpn_hp_dir="<Le_rep_ou_est_rpn_hp.pl_a_partir_de_votre_home>";
# Exemple: rpn_hp_dir="dev/perl/rpn_hp";

# Fin de configuration #####################################
############################################################

currentdir=$(pwd);
userhome=~;
rpn_hp_home="$userhome/$rpn_hp_dir";
cd $rpn_hp_home;
./rpn_hp.pl;
cd $currentdir;
